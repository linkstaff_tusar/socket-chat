var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var admin = require('firebase-admin');
const { connected } = require('process');

// Fetch the service account key JSON file contents
var serviceAccount = require("./service/serviceAccountKey.json");

// Initialize the app with a service account, granting admin privileges
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://matchmaker-a4194.firebaseio.com'
});

var db = admin.database();
var ref = db.ref("chats");
var port = process.env.PORT || 3000;
var user = "";
app.get('/:name', function(req, res){
  res.sendFile(__dirname + '/index.html');  
});

// app.get('/:name', function(req, res) {
//   user = req.params.name; 
//   res.sendFile(__dirname + '/index.html');  
// });
app.get('', function(req, res) {
  user = req.params.name; 
  // console.log(log.params.name); 
  res.sendFile(__dirname + '/register.html');  
});
io.on('connection', function(socket){
  console.log(user + 'is connected');
  socket.on('kotha', function(data){
    console.log('an user connected');
    let ctime = new Date().toISOString();
    io.sockets.emit('kotha',
    {
      message: data.message,
      handle:  data.handle,
    });
    ref.push({
      message: data.message,
      sender: data.handle,
      time: ctime
    });
    console.log(data);
  });
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
});
http.listen(port, function(){
  console.log('listening on *:' + port);
});
